'use strict';

const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const SchemaTypes = Schema.Types;


const ComponentSchema = new Schema({
    elementName: {
        type: String,
        required: true
    },
    properties: {
        type: String,
        required: false
    },
    children: {
        type: String,
        required: false
    },
    components: [{
        type: SchemaTypes.ObjectId,
        ref: 'Component',
        required: false
    }],
    isBreakingLayout: {
        type: Boolean,
        default: false
    },
    breakingLayoutMarginLeft: {
        type: String,
        default: '0px'
    },
    breakingLayoutMarginRight: {
        type: String,
        default: '0px'
    },
    breakingLayoutMarginTop: {
        type: String,
        default: '0px'
    },
    breakingLayoutMarginBottom: {
        type: String,
        default: '0px'
    },
    versions: [{
        type: String,
        default: '0.0.1',
        required: false
    }]
});

module.exports = mongoose.model("Component", ComponentSchema);