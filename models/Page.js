const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const SchemaTypes = Schema.Types;


const PageSchema = new Schema({
    elementName: {
        type: String,
        required: true,
        default: 'Page'
    },
    properties: {
        type: String,
        required: false
    },
    children: {
        type: String,
        required: false
    },
    components: [{
        type: SchemaTypes.ObjectId,
        ref: 'Component',
        required: false
    }],
    route: {
        type: String,
        required: false,
        default: ''
    },
    project: {
        type: String,
        required: false,
        default: ''
    }
});

module.exports = mongoose.model("Page", PageSchema);