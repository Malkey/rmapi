FROM node:latest as debug
RUN mkdir -p /usr/src/api
COPY ./package.json /usr/src/api
WORKDIR /usr/src/api
RUN npm install
RUN npm install -g nodemon
COPY . /usr/src/api

EXPOSE 4001 9229
ENTRYPOINT ["nodemon","--inspect=0.0.0.0"]

FROM node:latest as prod
RUN mkdir -p /usr/src/api
COPY ./package.json /usr/src/api
WORKDIR /usr/src/api
RUN npm install
COPY . /usr/src/api
EXPOSE 4001
CMD ["npm", "start"]
